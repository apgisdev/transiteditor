class Route {
    constructor(routename, shortname, days, startDate, endDate) {
        this.RouteName = routename;
        this.RouteShortName = shortname;
        this.WeekDays = days;
        this.StartDate = startDate;
        this.EndDate = endDate;
        this.Patterns = [];
    }
}