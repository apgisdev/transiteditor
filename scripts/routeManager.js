var project = JSON.parse(sessionStorage.getItem('Project'));

function yes() {
  window.location.href = "../main.html";
}

function create() {

  var zip = new JSZip();
  zip.file("agency.txt", createAgencyFile());
  zip.file("stops.txt", createStopsFile());
  zip.file("routes.txt", createRoutesFile());
  zip.file("trips.txt", createTripFile());
  zip.file("stop_times.txt", createStopTimesFile());
  zip.file("calendar.txt", createCalendarFile());


  zip.generateAsync({
      type: "blob"
    })
    .then(function (content) {
      // see FileSaver.js
      saveAs(content, "example.zip");
    });
}

function createAgencyFile() {
  var agencyInfo = "agency_name,agency_url,agency_timezone\n";
  agencyInfo += project.AgencyName;
  agencyInfo += "," + project.AgencyURL;
  agencyInfo += "," + project.AgencyTimeZone;

  return agencyInfo;
}

function createStopsFile() {
  var stopsInfo = "stop_id,stop_name,stop_desc,stop_lat,stop_lon,stop_url,location_type,parent_station\n";

  project.ProjectRoutes.forEach(function (route) {
    var patterns = route.Patterns;
    patterns.forEach(function (pattern) {
      var points = pattern.StopsInOrder;
      points.forEach(function (point) {
        //console.log(point.attributes.stop_id);  //CHANGE TO X AND Y COORDINATES, MIGHT GET RID OF DUPLICATES
        if (!stopsInfo.includes(point.geometry.y.toString()) && !stopsInfo.includes(point.geometry.x.toString())) { //can use to get rid of duplicates, not necessary
          stopsInfo += point.attributes.stop_id; //and might cause problems if used
          stopsInfo += "," + point.attributes.stop_name;
          stopsInfo += ","; // + point.attributes.stop_desc;
          stopsInfo += "," + point.geometry.y.toString();
          stopsInfo += "," + point.geometry.x.toString();
          stopsInfo += ",,,\n"
        }
      });
    });
  });
  return stopsInfo;
}

function toLetters(num) {
  "use strict";
  var mod = num % 26,
    pow = num / 26 | 0,
    out = mod ? String.fromCharCode(64 + mod) : (--pow, 'Z');
  return pow ? toLetters(pow) + out : out;
}

function createRoutesFile() {
  var routeInfo = "route_id,route_short_name,route_long_name,route_desc,route_type\n";
  var routeID = 0;

  project.ProjectRoutes.forEach(function (route) {
    routeID += 1;
    routeInfo += toLetters(routeID);
    routeInfo += "," + route.RouteShortName;
    routeInfo += "," + route.RouteName;
    routeInfo += ",,3\n"
  });
  return routeInfo;
}

function createTripFile() {
  var tripInfo = "route_id,service_id,trip_id\n";
  routeID = 0;

  project.ProjectRoutes.forEach(function (route) {
    routeID += 1;

    route.Patterns.forEach(function (pattern) {
      tripInfo += toLetters(routeID);
      tripInfo += "," + pattern.PatternName + "Cal" + "," + pattern.PatternName + "\n";
    });
  });
  return tripInfo;
}

function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('');
}

function createCalendarFile() {
  var calendarInfo = "service_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,start_date,end_date\n";

  project.ProjectRoutes.forEach(function (route) {
    route.Patterns.forEach(function (pattern) {
      var mon = 0,
        tue = 0,
        wed = 0,
        thu = 0,
        fri = 0,
        sat = 0,
        sun = 0;
      pattern.DayOfWeek.forEach(function (day) {
        if (day == "mon")
          mon = 1;
        if (day == "tue")
          tue = 1;
        if (day == "wed")
          wed = 1;
        if (day == "thu")
          thu = 1;
        if (day == "fri")
          fri = 1;
        if (day == "sat")
          sat = 1;
        if (day == "sun")
          sun = 1;
      });
      calendarInfo += pattern.PatternName + "Cal" + "," + mon + "," + tue + "," + wed + "," + thu + "," +
        fri + "," +
        sat +
        "," + sun + "," + formatDate(route.StartDate) + "," + formatDate(route.EndDate) + "\n"
    });

  });
  return calendarInfo;
}

function createStopTimesFile() {
  var stopTimesInfo = "trip_id,arrival_time,departure_time,stop_id,stop_sequence\n";
  var circularOrNon = sessionStorage.getItem("CircularOrNon");

  project.ProjectRoutes.forEach(function (route) {
    route.Patterns.forEach(function (pattern) {

      var firstStopID = pattern.StopsInOrder[0].attributes.stop_id; // error " pattern.StopsInOrder[0]" is undefined
      var firstStopXandY = [pattern.StopsInOrder[0].geometry.x, pattern.StopsInOrder[0].geometry.y];
      var lastStopID = pattern.StopsInOrder[(pattern.StopsInOrder.length) - 1].attributes.stop_id;
      var lastStopXandY = [pattern.StopsInOrder[(pattern.StopsInOrder.length) - 1].geometry.x, pattern.StopsInOrder[
        (pattern.StopsInOrder.length) - 1].geometry.y];
      startTimeArr = pattern.StartTime.split(":");
      endTimeArr = pattern.EndTime.split(":");
      var numberOfStops = pattern.StopsInOrder.length;

      if (circularOrNon == "circular") {

      }

      var patternDuration = [endTimeArr[0] - startTimeArr[0], endTimeArr[1] - startTimeArr[1]];
      patternDuration = (patternDuration[0] + (patternDuration[1] / 100)) * 60;
      var timeBetweenStops;

      var currentTime = [];
      var stopSequence = 0;
      var currentStop = 0;
      var timeOfStop = [parseInt(startTimeArr[0]), parseInt(startTimeArr[1]), 0];

      if (pattern.NumberOfTrips == 1 && pattern.TripLength == 1) {

        timeBetweenStops = (patternDuration / pattern.NumberOfTrips) / (numberOfStops - 1);
        pattern.StopsInOrder.forEach(function (stop) {
          if (patternDuration - timeBetweenStops < 0) {
            // do nothing
          } else {
            if (stopSequence % pattern.StopsInOrder.length == 0) {
              pattern.StopTimes.push([stop.geometry.x, stop.geometry.y, addMinutesToTime([parseInt(
                startTimeArr[0]), parseInt(startTimeArr[1])], (timeBetweenStops * pattern.StopsInOrder
                .length) * currentStop)]);
            }
            stopSequence += 1;
            if (stopSequence % pattern.StopsInOrder.length == 0) {
              pattern.StopTimes.push([stop.geometry.x, stop.geometry.y, addMinutesToTime([parseInt(
                startTimeArr[0]), parseInt(startTimeArr[1])], (timeBetweenStops * pattern.StopsInOrder
                .length) * currentStop + (
                timeBetweenStops * (stopSequence / (currentStop + 1)) - timeBetweenStops))]);
            }

            stopTimesInfo += pattern.PatternName + ",";
            var found = false;
            pattern.StopTimes.forEach(function (stopTime) {


              var time = addMinutesToTime(stopTime[2], 0);
              if (stopTime[0] == stop.geometry.x && stopTime[1] == stop.geometry.y) {

                stopTimesInfo += time[0] + ":" + pad(time[1]) + ":" + pad(time[2]) + "," + time[0] +
                  ":" + pad(
                    time[1]) +
                  ":" + pad(time[2]) + "," + stop.attributes
                  .stop_id + "," + stopSequence + "\n";
                timeOfStop = addMinutesToTime(timeOfStop, timeBetweenStops);
                pattern.StopTimes.splice(pattern.StopTimes.indexOf(stopTime));
                found = true;
              }
            });
            if (found == false) {
              stopTimesInfo += ",," + stop.attributes.stop_id + "," + stopSequence + "\n";
              timeOfStop = addMinutesToTime(timeOfStop, timeBetweenStops);
            }
          }
        });
        return;
      } else if (pattern.NumberOfTrips != 0) {
        //console.log(patternDuration + " / " + pattern.NumberOfTrips + " ) / " + numberOfStops);
        timeBetweenStops = (patternDuration / pattern.NumberOfTrips) / numberOfStops;
      } else {
        timeBetweenStops = parseInt(pattern.TripLength) / (parseInt(numberOfStops));
      }
      //console.log(timeBetweenStops);


      while (patternDuration - timeBetweenStops * numberOfStops > 0) {
        pattern.StopsInOrder.forEach(function (stop) {
          patternDuration = patternDuration - timeBetweenStops;
          if (stopSequence % pattern.StopsInOrder.length == 0) {
            pattern.StopTimes.push([stop.geometry.x, stop.geometry.y, addMinutesToTime([parseInt(
              startTimeArr[0]), parseInt(startTimeArr[1])], (timeBetweenStops * pattern.StopsInOrder
              .length) * currentStop)]);
          }
          stopSequence += 1;
          if (stopSequence % pattern.StopsInOrder.length == 0) {
            pattern.StopTimes.push([stop.geometry.x, stop.geometry.y, addMinutesToTime([parseInt(
              startTimeArr[0]), parseInt(startTimeArr[1])], (timeBetweenStops * pattern.StopsInOrder
              .length) * currentStop + (
              timeBetweenStops * (stopSequence / (currentStop + 1)) - timeBetweenStops))]);
          }


          stopTimesInfo += pattern.PatternName + ",";
          var found = false;
          pattern.StopTimes.forEach(function (stopTime) {
            if (stopTime[0] == stop.geometry.x && stopTime[1] == stop.geometry.y) {


              var time = stopTime[2];
              if (stopTime[0] == stop.geometry.x && stopTime[1] == stop.geometry.y) {
                stopTimesInfo += time[0] + ":" + pad(time[1]) + ":" + pad(time[2]) + "," + time[0] +
                  ":" + pad(
                    time[1]) +
                  ":" + pad(time[2]) + "," + stop.attributes
                  .stop_id + "," + stopSequence + "\n";
                timeOfStop = addMinutesToTime(timeOfStop, timeBetweenStops);
                if (stop.attributes.stop_id == firstStopID || stop.attributes.stop_id ==
                  lastStopID) {
                  pattern.StopTimes.splice(pattern.StopTimes.indexOf(stopTime));
                } else {
                  stopTime[2] = addMinutesToTime(stopTime[2], timeBetweenStops * pattern.StopsInOrder
                    .length);
                }
                found = true;
              }
            }
          });
          if (found == false) {
            stopTimesInfo += ",," + stop.attributes.stop_id + "," + stopSequence + "\n";
            timeOfStop = addMinutesToTime(timeOfStop, timeBetweenStops);
          }


        });
        currentStop += 1;
      }
      pattern.StopsInOrder.forEach(function (stop) {
        if (patternDuration - timeBetweenStops < 0) {
          // do nothing
        } else {
          if (stopSequence % pattern.StopsInOrder.length == 0) {
            pattern.StopTimes.push([stop.geometry.x, stop.geometry.y, addMinutesToTime([parseInt(
              startTimeArr[0]), parseInt(startTimeArr[1])], (timeBetweenStops * pattern.StopsInOrder
              .length) * currentStop)]);
          }
          stopSequence += 1;
          if (stopSequence % pattern.StopsInOrder.length == 0) {
            pattern.StopTimes.push([stop.geometry.x, stop.geometry.y, addMinutesToTime([parseInt(
              startTimeArr[0]), parseInt(startTimeArr[1])], (timeBetweenStops * pattern.StopsInOrder
              .length) * currentStop + (
              timeBetweenStops * (stopSequence / (currentStop + 1)) - timeBetweenStops))]);
          }

          stopTimesInfo += pattern.PatternName + ",";
          var found = false;
          pattern.StopTimes.forEach(function (stopTime) {


            var time = addMinutesToTime(stopTime[2], 0);
            if (stopTime[0] == stop.geometry.x && stopTime[1] == stop.geometry.y) {

              stopTimesInfo += time[0] + ":" + pad(time[1]) + ":" + pad(time[2]) + "," + time[0] +
                ":" + pad(
                  time[1]) +
                ":" + pad(time[2]) + "," + stop.attributes
                .stop_id + "," + stopSequence + "\n";
              timeOfStop = addMinutesToTime(timeOfStop, timeBetweenStops);
              pattern.StopTimes.splice(pattern.StopTimes.indexOf(stopTime));
              found = true;
            }
          });
          if (found == false) {
            stopTimesInfo += ",," + stop.attributes.stop_id + "," + stopSequence + "\n";
            timeOfStop = addMinutesToTime(timeOfStop, timeBetweenStops);
          }
        }
      });

      stopTimesInfo += pattern.PatternName + "," + endTimeArr[0] + ":" + endTimeArr[1] + ":" + "00," +
        endTimeArr[0] +
        ":" + endTimeArr[1] +
        ":" + "00," + pattern.StopsInOrder[0].attributes.stop_id + "," + (parseInt(stopSequence) + 1) +
        "\n";
    });

  });


  return stopTimesInfo;
}


function pad(d) {
  return (d < 10) ? '0' + d.toString() : d.toString();
}

function addMinutesToTime(time, minutes) {
  if (time[2] == null) {
    time[2] = 0;
  }

  //console.log(time[0] + ":" + time[1] + ":" + time[2] + "    + " + minutes);

  var seconds = (minutes - Math.floor(minutes)) * 60;
  time[2] += seconds;
  time[1] = Math.round(time[1]) + Math.round(minutes);
  if (seconds >= 60) {
    time[1] += time[2];
    time[2] += -60;
  }
  var val = (Math.round(time[1]) - Math.round(time[1])) * 60;
  while (time[1] >= 60) {
    time[0] = time[0] + 1;
    time[1] = Math.round(time[1] - 60);
    var val = (Math.round(time[1]) - Math.round(time[1])) * 60;
  }
  return [Math.floor(time[0]), Math.floor(time[1]), parseInt(time[2])];
}
