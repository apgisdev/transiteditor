var submittedStopsJSON = false;
// used to be able to loop through the check box id's
var daysOfWeek = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];

// runs the modal and sets all the check boxes to be unchecked,
// ASSUMING that you have saved metadata to create a project
// AND ASSUMING that you have submitted a file of stops
// function popUpWindow() {
function togglePopUpWindow(dir) {
  var x = document.getElementById("modal");
  if (dir === "ON"){
    if (JSON.parse(sessionStorage.getItem('Project')) == null) {
      alert("ERROR: Must have a project saved.");
    } else if (sessionStorage.getItem("UploadedStops") == null) {
      var choice = prompt("You haven't submitted a file of stops, are you sure you want to continue? (Y/N)");
      if (choice == "Y" || choice == "y") {
        window.location.href = "./views/tripPattern.html";
      }
    } else {
      toggleAllCheckBoxes("OFF");
      x.style.display = "block";
    }
    fillCurrentYear();
  } else if ( dir === "OFF"){
    x.style.display = "none";
  }

}

// sets the check boxes in the pop up window to equal nothing
// so they dont stay marked

function toggleAllCheckBoxes(dir) {
  var ids = ["mon","tue","wed","thu","fri","sat","sun"]
  for (var i = 0; i < ids.length; i++){
    if (dir === "OFF"){
      document.getElementById(ids[i]).checked = "";
    } else if (dir === "ON"){
      document.getElementById(ids[i]).checked = "checked";
    }

  }
  if (dir === "OFF"){
    document.getElementById("RouteName").value = "";
    document.getElementById("RouteShortName").value = "";
  }
}


var Routes = [];
var PatternNames = [];

sessionStorage.setItem('Routes', JSON.stringify(Routes));


// a test to make sure the right click on the markers works
function testContextMenuClick() {
  alert("You pressed a button in the context menu.");
}

// Saves the metadata that was enetered and saves it to session storage
function SaveMetadata() {
  // checks to make sure there is not another project already
  if (JSON.parse(sessionStorage.getItem('Project')) != null) {

    // Let user know the project is already save
    // alert("ERROR: Already have a project saved.");

    // log to console for developer
    console.log("JSON.parse(sessionStorage.getItem('Project')) = " + JSON.stringify(JSON.parse(sessionStorage.getItem('Project'))));

    // print error to DOM
    printToDOMID("deposit_notice_saved", "Error. Project already saved");
    return;

  } else {
    // gets metadata entered and assigns to variables
    var projectName = document.getElementById('ProjectName').value;
    var agencyName = document.getElementById('AgencyName').value;
    var maintainerName = document.getElementById('MaintainersName').value;
    var agencyURL = document.getElementById('AgencyURL').value;
    var agencyTimeZone = document.getElementById('AgencyTimeZone').value;


    var project = null;

    // makes sure project name and angency name has values enetered
    if (projectName == "" || agencyName == "" || agencyURL == "" || agencyTimeZone == "") {
      alert("Must have values entered for non optional fields.");
      printToDOMID("deposit_notice_saved", "Missing fields");
    } else {
      project = new Project(projectName, agencyName, agencyURL, agencyTimeZone, maintainerName);
    }

    // if the project is null than save the project to session storage
    if (project == null) {
      alert("ERROR: project = null");
    } else {
      sessionStorage.setItem('Project', JSON.stringify(project));
    }
  }
  // Make the DOM say the stuff was saved
  printToDOMID("deposit_notice_saved", "Saved");
}

// print some text to the DOM at a specific ID
function printToDOMID(depositID, text){
  // if the depositID or text is not a string exit without printing
  if (typeof text !== "string" || typeof depositID !== "string"){
    console.error("Error: depositID not a string")
    return;
  }
  // print
  document.getElementById(depositID).innerHTML = text
}

var stopQueue = [];

// Saves all data given by user and saves it to the project and session storage,
// and assuming their are no errors it will redirect to views/tripPattern.html
function saveAndGoToTripPattern() {
  // represents if the short name of route and standard name of route are duplicated
  var longDuplicate = false;
  var shortDuplicate = false;

  var project = JSON.parse(sessionStorage.getItem('Project'))

  // Checks if the entered route name and short route name is found anywhere else
  // in the current project
  project.ProjectRoutes.forEach(function (route) {
    if (document.getElementById("RouteName").value == route.RouteName) {
      longDuplicate = true;
      // return;
    }

    if (document.getElementById("RouteShortName").value == route.RouteShortName) {
      shortDuplicate = true;
      // return;
    }
  });

  // Checks to make sure the user entered values in the elements RouteName and RouteShortName
  if (document.getElementById("RouteName").value == "" || document.getElementById("RouteShortName").value == "") {
    alert("ERROR: You must enter a route name and short name");
  } else if (longDuplicate == true || shortDuplicate == true) {
    if (longDuplicate == true) {
      alert("ERROR: That route name already exists.");
    }

    if (shortDuplicate == true) {
      alert("ERROR: That route short name already exists.");
    }

    return;
  } else {
    // add the newly inputted route name and save it to local storage
    Routes = JSON.parse(sessionStorage.getItem('Routes'));


    var days = [];

    // creates an array of week days for each checkbox pressed
    daysOfWeek.forEach(function (day) {
      if (document.getElementById(day).checked == true) {
        days.push(day);
      }
    });
    // makes sure the user selected a day for the route to run,
    // and if it does then it creates a new route object with
    // the given information
    if (days.length == 0) {
      alert("ERROR: The route must run on at least one day.");
    } else {
      newRoute = new Route(document.getElementById("RouteName").value,
        document.getElementById("RouteShortName").value,
        days,
        document.getElementById("StartDate").value,
        document.getElementById("EndDate").value);

      Routes.push(newRoute);

      //sets the  route the route just made
      var currentRoute = document.getElementById("RouteName").value;
      sessionStorage.setItem('CurrentRoute', currentRoute);

      sessionStorage.setItem('Routes', JSON.stringify(Routes));

      // add the new route to the project and save it
      var project = JSON.parse(sessionStorage.getItem('Project'));
      project.ProjectRoutes.push(newRoute);
      sessionStorage.setItem('Project', JSON.stringify(project));

      // go to trip pattern page
      window.location.href = "./views/tripPattern.html";
    }
  }

}

// Checks to make sure that you have uploaded stops and if you have
// then redirects you to views/tripPattern.html
function goToTripPattern() {
  if (sessionStorage.getItem("UploadedStops") == null) {
    var choice = prompt("You haven't submitted a file of stops, are you sure you want to continue? (Y/N)");
    if (choice == "Y" || choice == "y") {
      window.location.href = "./views/tripPattern.html";
    } else {
      return;
    }
  }
  window.location.href = "./views/tripPattern.html";
}

// Using the file intermediateStorage.js it creates the intermediate GTP
// and GTR files, zips them up, and gives them to the user
function giveUserIntermediateFiles() {
  if (JSON.parse(sessionStorage.getItem('Project')) == null) {
    alert("ERROR: Must have a project saved.");
    return;
  }

  var intermediateFiles = createIntermediateFiles();

  var zip = new JSZip();
  zip.file("GTP.json", JSON.stringify(intermediateFiles[0]));
  zip.file("GTR.json", JSON.stringify(intermediateFiles[1]));

  zip.generateAsync({
      type: "blob"
    })
    .then(function (content) {
      // see FileSaver.js
      saveAs(content, "intermediate.zip");
    });
}

// variables that will hold the data from a user given file
var uploadedGTRFile = "";
var uploadedGTPFile = "";
var uploadedStopsFile = "";

//================================================================================//
// Note for the following handleFileSelect*** functions:                          //
//  trying to combine them into one function in order to avoid repeating myself   //
//  creates errors, so need to keep them as seperate functions                    //
//================================================================================//

var gtrSubmitted = false;
var gtpSubmitted = false;

// Runs when the user presses the "Submit GTP" button.
// it checks to see if the file is valid and if it does then calls
// the recievecedTextGTP function
function handleFileSelectGTP() {
  if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
    alert('The File APIs are not fully supported in this browser.');
    return;
  }

  input = document.getElementById('uploadedGTPFile');
  if (!input) {
    alert("Um, couldn't find the fileinput element.");
  } else if (!input.files) {
    alert("This browser doesn't seem to support the `files` property of file inputs.");
  } else if (!input.files[0]) {
    alert("Please select a file before clicking 'Submit'");
  } else {
    file2 = input.files[0];
    fr = new FileReader();
    fr.onload = receivedTextGTP;
    //fr.readAsText(file);
    fr.readAsText(file2);
  }
}

// Sets uploaded GTP file to a string of the file submitted
function receivedTextGTP() {
  gtpSubmitted = true;
  uploadedGTPFile = fr.result;
}

// Runs when the user presses the "Submit GTR" button.
// it checks to see if the file is valid and if it does then calls
// the recievecedTextGTR function
function handleFileSelectGTR() {
  if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
    alert('The File APIs are not fully supported in this browser.');
    return;
  }

  input = document.getElementById('uploadedGTRFile');
  console.log(input.type);
  if (!input) {
    alert("Um, couldn't find the fileinput element.");
  } else if (!input.files) {
    alert("This browser doesn't seem to support the `files` property of file inputs.");
  } else if (!input.files[0]) {
    alert("Please select a file before clicking 'Submit'");
  } else {
    file1 = input.files[0];
    fr = new FileReader();
    fr.onload = receivedTextGTR;
    //fr.readAsText(file);
    fr.readAsText(file1);
  }
}

// Sets uploaded GTP file to a string of the file submitted
function receivedTextGTR() {
  gtrSubmitted = true;
  uploadedGTRFile = fr.result;
}

// Runs when the user presses the "Submit Stops File" button.
// it checks to see if the file is valid and if it does then calls
// the recievedTextStops function
function handleFileSelectStops() {
  console.log("handleFileSelectStops() fired");
  submittedStopsJSON = true;
  if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
    alert('The File APIs are not fully supported in this browser.');
    return;
  }

  input = document.getElementById('uploadedStopsFile');
  if (!input) {
    alert("Um, couldn't find the fileinput element.");
  } else if (!input.files) {
    alert("This browser doesn't seem to support the `files` property of file inputs.");
  } else if (!input.files[0]) {
    alert("Please select a file before clicking 'Submit'");
  } else {
    stopsFile = input.files[0];
    fr = new FileReader();
    fr.onload = receivedTextStops;
    //fr.readAsText(file);
    fr.readAsText(stopsFile);
  }
}

// Sets uploaded stops file to a string of the file submitted
function receivedTextStops() {
  // get the result from file read
  uploadedStopsFile = fr.result;

  // parse text into JSON for ease of
  uploadedJSON = JSON.parse(uploadedStopsFile);

  // save in sessionStorage object for use in other
  sessionStorage.setItem('UploadedStops', uploadedStopsFile);
}

var edit = false;

// Will send the user to views/tripPattern.html so they can edit an existing route or pattern
// Sets a session storage variable to true so views/tripPattern.html will be able to recognize
// whether to go straight into editing mode or not
function editExisting() {
  if (JSON.parse(sessionStorage.getItem('Project')) == null) {
    alert("ERROR: Must have a project saved.");
    return;
  }

  edit = true;
  sessionStorage.setItem('EditDeterminant', edit);
  goToTripPattern();
}

// Checks to see if project is valid, and if so asks for a route
// in which you want to edit, which then checks to see if the
// route name exists and if it does then sends user to tripPatttern.html
// with the route entered as the current route being editted
function addPattern() {
  var project = JSON.parse(sessionStorage.getItem('Project'));
  if (project == null) {
    alert("ERROR: No project made, therefore no routes exist to add to.");
    return;
  }
  console.log(project);
  var promptRouteName = prompt("What route do you want to add a pattern too?");

  project.ProjectRoutes.forEach(function (route) {
    if (route.RouteName == promptRouteName) {
      sessionStorage.setItem('CurrentRoute', promptRouteName);
      goToTripPattern();
    }
  });

  alert("ERROR: Route not found.");
}

function goToRouteManager() {
  var project = JSON.parse(sessionStorage.getItem('Project'));
  if (project == null) {
    alert("ERROR: No project made, therefore no routes exist to add to.");
    return;
  } else {
    window.location.href = "./views/routeManager.html";
  }
}

function fillCurrentYear() {
  var startTime = document.getElementById("StartDate");
  var endTime = document.getElementById("EndDate");
  var now = new Date();
  var oneYearFromNow = new Date((new Date()).getTime() + 365*24*60*60*1000);
  // console.log("now = " + now);
  // console.log("now.toISOString() = " + now.toISOString());
  // console.log("now.toISOString().splice(0,20) = " + now.toISOString().substring(0,10));
  // console.log("oneYearFromNow = " + oneYearFromNow);
  // console.log("startTime.value = " + startTime.value);
  var timeNow = now.toISOString().substring(0,10);
  var timeNowOneYearFromNow = oneYearFromNow.toISOString().substring(0,10);
  // console.log("timeNow = " + timeNow);
  // yyyy-mm-dd
  startTime.value = timeNow;
  endTime.value = timeNowOneYearFromNow;
  // console.log("startTime.value = " + startTime.value);
}

function selectAllDays() {
  toggleAllCheckBoxes("ON");
}
