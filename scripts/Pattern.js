// Pattern class to hold all necessary information about a route pattern
// The pattern class will be used inside the Route class where it will
// hold all the patterns in that particular route

class Pattern {

    constructor(patternName, dayOfWeek, startTime, endTime, numberOfTrips, tripLength) {
        this.PatternName = patternName;
        this.DayOfWeek = dayOfWeek;
        this.StartTime = startTime;
        this.EndTime = endTime;
        this.NumberOfTrips = numberOfTrips;
        this.TripLength = tripLength;
        this.StopTimes = [];
        this.StopsInOrder = [];
    }

}