class Project{

    constructor(projectName, agencyName, agencyURL, agencyTimeZone, maintainerName){
        this.ProjectName = projectName;
        this.AgencyName = agencyName;
        this.AgencyURL = agencyURL;
        this.AgencyTimeZone = agencyTimeZone;
        this.MaintainerName = maintainerName;
        this.ProjectRoutes = [];
    }
}