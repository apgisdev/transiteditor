function createIntermediateFiles() {
    var project = JSON.parse(sessionStorage.getItem('Project'));

    if (project == null) {
        alert("ERROR: No project exists to create intermediate data from");
        return;
    }

    FinalGTPInfo = JSON.parse(createGTPFile(project));
    console.log(createGTRFile(project));
    FinalGTRInfo = JSON.parse(createGTRFile(project));

    return [FinalGTPInfo, FinalGTRInfo];
}

// Creates the GTP file which will hold the metadata for the project
function createGTPFile(project) {

    var GTPInfo = "";

    GTPInfo += "{\n";

    GTPInfo += '"ProjectName": ' + '"' + project.ProjectName + '",\n';
    GTPInfo += '"AgencyName": ' + '"' + project.AgencyName + '",\n';
    GTPInfo += '"AgencyURL": ' + '"' + project.AgencyURL + '",\n';
    GTPInfo += '"AgencyTimeZone": ' + '"' + project.AgencyTimeZone + '",\n';
    GTPInfo += '"MaintainerName": ' + '"' + project.MaintainerName + '"\n';

    GTPInfo += "}";

    return GTPInfo;
}

function createGTRFile(project) {


    var GTRInfo = "";

    GTRInfo += "{\n";

    GTRInfo += '"ProjectRoutes": [';

    project.ProjectRoutes.forEach(function (route) {
        GTRInfo += "{\n";
        GTRInfo += '"RouteName": ' + '"' + route.RouteName + '",\n';
        GTRInfo += '"RouteShortName": ' + '"' + route.RouteShortName + '",\n';

        GTRInfo += '"WeekDays": [';
        route.WeekDays.forEach(function (day) {
            GTRInfo += '"' + day + '"';
            if (route.WeekDays.indexOf(day) != route.WeekDays.length - 1) {
                GTRInfo += ', ';
            }
        });
        GTRInfo += "],\n";

        GTRInfo += '"StartDate": ' + '"' + route.StartDate + '",\n';
        GTRInfo += '"EndDate": ' + '"' + route.EndDate + '",\n';

        GTRInfo += '"Patterns": [';

        route.Patterns.forEach(function (pattern) {
            GTRInfo += "{\n";
            GTRInfo += '"PatternName": ' + '"' + pattern.PatternName + '",\n';

            GTRInfo += '"DayOfWeek": [';
            pattern.DayOfWeek.forEach(function (day) {
                GTRInfo += '"' + day + '"';
                if (pattern.DayOfWeek.indexOf(day) != pattern.DayOfWeek.length - 1) {
                    GTRInfo += ', ';
                }
            });
            GTRInfo += "],\n";

            GTRInfo += '"StartTime": ' + '"' + pattern.StartTime + '",\n';
            GTRInfo += '"EndTime": ' + '"' + pattern.EndTime + '",\n';
            GTRInfo += '"NumberOfTrips": ' + '"' + pattern.NumberOfTrips + '",\n';
            GTRInfo += '"TripLength": ' + '"' + pattern.TripLength + '",\n';

            GTRInfo += '"StopTimes": [';
            pattern.StopTimes.forEach(function (time) {
                GTRInfo += '"' + time + '"';
                if (pattern.StopTimes.indexOf(time) != pattern.StopTimes.length - 1) {
                    GTRInfo += ', ';
                }
            });
            GTRInfo += '],\n';

            GTRInfo += '"StopsInOrder": [';
            pattern.StopsInOrder.forEach(function (stop) {
                GTRInfo += preparePoint(stop);
                if (pattern.StopsInOrder.indexOf(stop) != pattern.StopsInOrder.length - 1) {
                    GTRInfo += ', ';
                }
            });
            GTRInfo += ']\n';


            if (route.Patterns.indexOf(pattern) != route.Patterns.length - 1) {
                GTRInfo += '},';
            } else {
                GTRInfo += "}\n";
            }
        });

        GTRInfo += ']';

        if (project.ProjectRoutes.indexOf(route) != project.ProjectRoutes.length - 1) {
            GTRInfo += '},';
        } else {
            GTRInfo += "}\n";
        }
    });

    GTRInfo += "]\n";

    GTRInfo += "}";

    return GTRInfo;
}

function preparePoint(point) {
    var pointInfo = "";

    pointInfo += '{"attributes": {\n';

    // SEE IF WE NEED ALL ATTRIBUTES OR JUST THE REQUIRED ONES
    //pointInfo += '"FID": ' + point.attributes.FID + ',\n';
    //pointInfo += '"OID_": ' + point.attributes.OID_ + ',\n';
    pointInfo += '"stop_id": ' + point.attributes.stop_id + ',\n';
    //pointInfo += '"stop_url": ' + '"' + point.attributes.stop_url + '",\n';
    //pointInfo += '"stop_code": ' + point.attributes.stop_code + ',\n';
    pointInfo += '"stop_name": ' + '"' + point.attributes.stop_name + '",\n';
    //pointInfo += '"stop_desc": ' + '"' + point.attributes.stop_desc + '",\n';
    //pointInfo += '"parent_sta": ' + point.attributes.parent_sta + ',\n';
    //pointInfo += '"stop_timez": ' + '"' + point.attributes.stop_timez + '",\n';
    //pointInfo += '"wheelchair": ' + point.attributes.wheelchair + ',\n';
    //pointInfo += '"location_t": ' + '"' + point.attributes.location_t + '",\n';
    //pointInfo += '"zone_id": ' + '"' + point.attributes.zone_id + '",\n';
    pointInfo += '"stop_lon": ' + point.attributes.stop_lon + ',\n';
    pointInfo += '"stop_lat": ' + point.attributes.stop_lat + '\n';
    //pointInfo += '"point_x_dd": ' + point.attributes.point_x_dd + ',\n';
    //pointInfo += '"point_y_dd": ' + point.attributes.point_y_dd + ',\n';
    //pointInfo += '"DDLat": ' + '"' + point.attributes.DDLat + '",\n';
    //pointInfo += '"DDLon": ' + '"' + point.attributes.DDLon + '",\n';
    //pointInfo += '"ORIG_OID": ' + point.attributes.ORIG_OID + ',\n';
    //pointInfo += '"stop_order": ' + point.attributes.stop_order + '\n';

    pointInfo += '},\n"geometry": {\n';

    pointInfo += '"x": ' + point.geometry.x + ',\n';
    pointInfo += '"y": ' + point.geometry.y + '\n';

    pointInfo += '}}';

    return pointInfo;
}

function loadIntermediateFiles(GTP, GTR, gtpSubmitted, gtrSubmitted) {
    if (gtpSubmitted == false || gtrSubmitted == false) {
        alert("ERROR: You must submit both the gtp and gtr file to load intermediate files");
        return;
    }

    GTP = JSON.parse(GTP);
    GTR = JSON.parse(GTR);

    var project = new Project(GTP.ProjectName, GTP.AgencyName, GTP.AgencyURL, GTP.AgencyTimeZone, GTP.MaintainerName);

    GTR.ProjectRoutes.forEach(function (route) {
        project.ProjectRoutes.push(route);
    });

    sessionStorage.setItem("Project", JSON.stringify(project));
}