// var convert = require('xml-js');
// used to be able to loop through the check box id's
var daysOfWeek = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
var days = [];
var x = document.getElementById("modal");

var uploadedStops = JSON.parse(sessionStorage.getItem('UploadedStops'));

var map = null;

var fileType = "KML";
var stopQueue = [];

// fires after html is loaded into view
window.onload = function() {

  // initialize the map
  map = L.map('map', {
    contextmenu: true,
    contextmenuWidth: 140,
    contextmenuItems: [{
      text: 'Place Marker Here',
      callback: placeMarker
    }]
  }).setView([36.53, -87.35], 13);

  // load a tile layer
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Tiles by <a href="http://mapc.org">MAPC</a>, Data by <a href="http://mass.gov/mgis">MassGIS</a>',
    maxZoom: 17,
    minZoom: 9,
    contextmenu: false,
    contextmenuWidth: 140,
    hideOnSelect: true,
  }).addTo(map);

  var drawnItems = new L.FeatureGroup();
  map.addLayer(drawnItems);
  var drawControl = new L.Control.Draw({
    draw: {
      marker: false,
      polyline: false,
      rectangle: false,
      circle: false,
      circlemarker: false
    },
    edit: {
      featureGroup: drawnItems,
      edit: false
    }
  });
  map.addControl(drawControl);

  // create a feature layer for the markeres
  var markersLayer = L.featureGroup().addTo(map);

  // creates the custom marker information
  CustomMarker = L.Marker.extend({
    options: {
      busStopID: ' ',
      busStopName: ' ',
      latX: 0,
      lonY: 0,
      icon: L.icon({iconUrl: '../images/poke_ball.png',
        iconSize: [12, 12],
      })
    }
  });

  var redIcon = L.icon({
    iconUrl: '../node_modules/leaflet/dist/images/marker-icon-red.png'
    // iconUrl: '../images/poke_ball.png'
  });

  // Used to hide context menu when you click somewhere else
  map.on('click', function(e) {
    map.contextmenu.hide();
  });

  map.on('draw:created', function(e) {
    var type = e.layerType,
      layer = e.layer;
    if (type === 'polygon') {
      markersLayer.eachLayer(function(marker) {
        if (isMarkerInsidePolygon(marker, layer) == true) {
          stopQueue.push(marker.options.point);
          marker.setIcon(redIcon);
        }
      });

    }
  });

  // When a marker is double clicked, changes the marker color to red and adds the
  // marker to the stopQueue Array

  markersLayer.on("dblclick", function(event) {
    var clickedMarker = event.layer;
    uploadedStops.features.forEach(function(point) {
      if (clickedMarker.options.latX == point.geometry.x &&
        clickedMarker.options.lonY == point.geometry.y) {
        stopQueue.push(point);
        clickedMarker.setIcon(redIcon);
      }
    });
  });

  seen = [];

  loadStops(markersLayer);
  popUpWindow();
}

// runs the modal and sets all the check boxes to be unchecked
function popUpWindow() {
  var x = document.getElementById("modal");
  toggleAllCheckBoxes("OFF");
  x.style.display = "block";

}

var circularOrNon = "";
// sets the modal div style display to none so it will no longer show
function closePopUpWindow() {
  // modal is the html pop up window

  var numberOfTrips = parseInt(document.getElementById("NumberOfTrips").value);
  var lengthOfTrips = parseInt(document.getElementById("LengthOfTrips").value);

  console.log("numberOfTrips = '" + numberOfTrips +"'");
  console.log("lengthOfTrips = '" + lengthOfTrips + "'");

  console.log("typeof numberOfTrips = " + typeof numberOfTrips);
  console.log("typeof lengthOfTrips = " + typeof lengthOfTrips);

  if ( numberOfTrips === 0 || lengthOfTrips === 0 || numberOfTrips === "" || lengthOfTrips === ""){
    alert("Please Enter the number of trips taken in this route and/or the lenth of each trip");
    return;
  }

  // Checks to see if the user wants the route to be circular or not
  //    -Circular: Will begin and stop at the same stop, can have multiple trips
  //    -Non: Will begin and stop at different stops, will only have ONE trip
  try {
    circularOrNon = $('input[name=circularCheckBox]:checked').val();
  } catch (e) {
    console.error("error looking at circularCheckBox");
  }

  if (circularOrNon == null) {
    alert("ERROR: Must chose either curcular or non-circular pattern.");
    return;
  }

  sessionStorage.setItem("CircularOrNon", circularOrNon);

  // gets the start times and end times and puts them in an array of the following
  // format: [HH, MM]
  var startTime = document.getElementById("StartTime").value.split(":");
  var endTime = document.getElementById("EndTime").value.split(":");

  // Checks to see if the user didnt enter anything in either input
  if (startTime[0] == "" || endTime[0] == "") {
    alert("ERROR: Must enter a stop time and end time");
    return;
  }

  // Makes sure that the start time of a route is never after the entered end time
  if (document.getElementById("StartTime").value > document.getElementById("EndTime").value) {
    alert("ERROR: The start time cannot be later than the end time.");
    return;
  }


  // number of minutes between the start and end time
  // (end-hour - start-hour) * minutes-in-one-hour + (end-minute - start-minute)
  var minutesBetween = (endTime[0] - startTime[0]) * 60 + Math.abs(endTime[1] - startTime[1]);

  // adds the string abbreviation to each day of week to the array of days
  // that will be used when creating a route object
  daysOfWeek.forEach(function(day) {
    if (document.getElementById(day).checked == true) {
      days.push(day);
    }
  });

  var duplicate = false;

  // The following if...else if structure will check to make sure the user inputs are valid,
  // and if so then it will close the pop up window
  if (document.getElementById("PatternName").value == "") {
    alert("ERROR: You must enter a pattern name");
  } else if (document.getElementById("NumberOfTrips").value == 0 && document.getElementById("LengthOfTrips").value ==
    0) {
    alert("ERROR: You must enter either the number of trips OR the length of trips");
  } else {
    project.ProjectRoutes.forEach(function(route) {
      route.Patterns.forEach(function(pattern) {
        if (pattern.PatternName == document.getElementById("PatternName").value) {
          alert("ERROR: This pattern name already exists in this route or another one.");
          duplicate = true;
        }
      });
    });

    // console.log("minutesBetween = " + minutesBetween);
    // console.log("numberOfTrips = " + numberOfTrips);
    // console.log("lengthOfTrips = " + lengthOfTrips);
    //
    // console.log("minutesBetween % numberOfTrips = " + (minutesBetween % numberOfTrips));
    // console.log("minutesBetween % lengthOfTrips = " + (minutesBetween % lengthOfTrips));

    if (duplicate === false) {
      if (minutesBetween % numberOfTrips != 0) {
        alert("ERROR: Number of trips and amount of time for the pattern is not evenly divisible");
        return;
      }
      if (minutesBetween % lengthOfTrips != 0) {
        alert("ERROR: Length of trips and amount of time for the pattern is not evenly divisible");
        return;
      }
      document.getElementById("modal").style.display = "none";
    }
  }
  loadShapePolylines();
}

var editDeterminant = sessionStorage.getItem("EditDeterminant");

// If the edit determinant is true than the program will automatically run the
// function that begins the process of editing and existing trip pattern
if (editDeterminant == "true") {
  EditExistingPattern();
}

var editing = false;
var existingPatternName = "";

// Prompts the user what route and pattern name and then will load up that pattern
// if it actual exists
function EditExistingPattern() {
  var routeFound = false,
    patternFound = false;

  var chosenRoute = prompt("What route is the pattern in?");
  var chosenPattern = prompt("What is the pattern you want to edit?");

  project.ProjectRoutes.forEach(function(route) {
    if (route.RouteName == chosenRoute) {
      routeFound = true;
      route.Patterns.forEach(function(pattern) {
        if (pattern.PatternName == chosenPattern) {
          patternFound = true;
          existingPatternName = pattern.PatternName;
          alert("found");
          editing = true;
          x.style.display = "none";
        }
      });
    }
  });

  if (routeFound == false) {
    alert("ERROR: route does not exist");
    return;
  } else if (patternFound == false) {
    alert("ERROR: pattern does not exist");
    return;
  }

}

// sets the check boxes in the pop up window to equal nothing
// so they dont stay marked
function toggleAllCheckBoxes(dir) { 
  var arr = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];

    for (var i = 0; i < arr.length; i++){
      if (dir === "OFF"){
        document.getElementById(arr[i]).checked = "";
      } else if (dir === "ON") {
        document.getElementById(arr[i]).checked = "checked";
      }
    }
    // document.getElementById("circularCheckBox").checked = "";
    document.getElementById("PatternName").value = "";
    document.getElementById("NumberOfTrips").value = 0;
    document.getElementById("LengthOfTrips").value = 0;
  }
}

function testContextMenuClick() {
  alert("You pressed a button in the context menu.");
}

// makes the popup window show as soon as the user navigates to the page

// popUpWindow();

// Checks if the markers exists inside the given polygon
// returns true if it exists, false if not
function isMarkerInsidePolygon(marker, poly) {
  var inside = false;
  var x = marker.getLatLng().lat,
    y = marker.getLatLng().lng;
  for (var ii = 0; ii < poly.getLatLngs().length; ii++) {
    var polyPoints = poly.getLatLngs()[ii];
    for (var i = 0, j = polyPoints.length - 1; i < polyPoints.length; j = i++) {
      var xi = polyPoints[i].lat,
        yi = polyPoints[i].lng;
      var xj = polyPoints[j].lat,
        yj = polyPoints[j].lng;

      var intersect = ((yi > y) != (yj > y)) &&
        (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
      if (intersect) inside = !inside;
    }
  }

  return inside;
};

// load stops from jquery/dist/jquery
function loadStops(markersLayer) {
  var jsonFeatures = [];
  var uploadedStops = JSON.parse(sessionStorage.getItem('UploadedStops'));
  uploadedStops.features.forEach(function(point) {

    var markerData = "";
    var lat = parseFloat(point.geometry.y);
    var lon = parseFloat(point.geometry.x);
    if (isNaN(lat) == true || isNaN(lon) == true) {
      return;
    }
    var text = parseInt(point.attributes.stop_id);
    var name = point.attributes.stop_name;
    var latlng = L.latLng({
      lat: lat,
      lng: lon
    });

    var popupText = text.toString() + "<br>" + name.toString() + "<br>" + lat.toString() + "<br>" + lon.toString();

    var marker = new CustomMarker(latlng, {
      contextmenu: true,
      contextmenuInheritItems: false,
      contextmenuItems: [{
        text: 'test',
        callback: testContextMenuClick,
        index: 0
      }]
    }, {
      // The marker custom propertys: X and Y coordinates of the point from the JSON file
      customProperty: popupText,
      latX: 0,
      lonY: 0
    });

    //sets the markeres custom data
    marker.options.busStopID = (point.attributes.stop_id).toString();
    marker.options.busStopName = (point.attributes.stop_name).toString();
    marker.options.latX = point.geometry.x;
    marker.options.lonY = point.geometry.y;
    marker.options.point = point;
    marker.addTo(markersLayer);

    marker.bindPopup(function() {
      return " " + popupText;
    });
  });
}

// saves data and relocates you to stop series page
function goToStopSeries() {
  if (editing == true) {
    sessionStorage.setItem("Project", JSON.stringify(project));
    sessionStorage.setItem("StopQueue", JSON.stringify(stopQueue));
    sessionStorage.setItem("CurrentPattern", existingPatternName);
    window.location.href = "./stopSeries.html";
  } else if (circularOrNon == "non-circular") {
    alert("non");
    var newPattern = new Pattern(document.getElementById("PatternName").value,
      days,
      document.getElementById("StartTime").value,
      document.getElementById("EndTime").value,
      1, 1);

    // find the current route and load the new pattern
    project.ProjectRoutes.forEach(function(route) {
      if (route.RouteName == CurrentRoute) {
        route.Patterns.push(newPattern);
      }
    });
    // fixes typescript cyclic error when stringifying
    var replacer = function(key, value) {
      if (value != null && typeof value == "object") {
        if (seen.indexOf(value) >= 0) {
          return;
        }
        seen.push(value);
      }
      return value;
    };
    // saves all changes to variables taken from session storage
    sessionStorage.setItem("Project", JSON.stringify(project, replacer));
    sessionStorage.setItem("StopQueue", JSON.stringify(stopQueue, replacer));
    sessionStorage.setItem("CurrentPattern", document.getElementById("PatternName").value);
    window.location.href = "./stopSeries.html";
  } else {
    // creates a new pattern object with all the required parameters for the cuonstructor
    var newPattern = new Pattern(document.getElementById("PatternName").value,
      days,
      document.getElementById("StartTime").value,
      document.getElementById("EndTime").value,
      document.getElementById("NumberOfTrips").value,
      document.getElementById("LengthOfTrips").value);

    // find the current route and load the new pattern
    project.ProjectRoutes.forEach(function(route) {
      if (route.RouteName == CurrentRoute) {
        route.Patterns.push(newPattern);
      }
    });
    // saves all changes to variables taken from session storage
    sessionStorage.setItem("Project", JSON.stringify(project, replacer));
    sessionStorage.setItem("StopQueue", JSON.stringify(stopQueue, replacer));
    sessionStorage.setItem("CurrentPattern", document.getElementById("PatternName").value);
    window.location.href = "./stopSeries.html";
  }
}

function receivedTextStops() {
  // get the result from file read
  uploadedStopsFile = fr.result;

  // parse text into JSON for ease of
  uploadedJSON = JSON.parse(uploadedStopsFile);

  // console.log(JSON.stringify(uploadedJSON, null, 2));

  // save in sessionStorage object for use in other
  sessionStorage.setItem('UploadedJSON', uploadedJSONFile);
}

function loadShapePolylines(){
  console.log("loadShapePolylines()");
  polylines = [];
  index = 0;
  // var polyline = sessionStorage.getItem('polyline0', polylines);
  // console.log("polyline = " + polyline);

  polylineArr = []
  point = null;

  while (polyline !== null){
    try{
      polyline = sessionStorage.getItem('polyline'+index, polylines).split(",");
    } catch (e) {
      console.error("polyline null");
      break;
    }
    // console.log("polyline = " + polyline);
    index++;

    for (var i = 0; i < polyline.length; i += 2){
      point = [polyline[i],polyline[i+1]]
      polylineArr.push(point)
    }

    // console.log("polylineArr = " + JSON.stringify(polylineArr,null,2));

    var polylineToMap = L.polyline(polylineArr, {
      color: 'red'
    }).addTo(map);

    polylineArr = [];
  }

  var polyline = null;
}

// get the file from the input the file by type from the input location
function getFile(){

  console.log("fileSubmit");

  submittedStopsJSON = true;

  if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
    alert('The File APIs are not fully supported in this browser.');
    return;
  }

  input = document.getElementById("fileSubmit");

    if (!input) {
      alert("Um, couldn't find the fileinput element.");
    } else if (!input.files) {
      alert("This browser doesn't seem to support the `files` property of file inputs.");
    } else if (!input.files[0]) {
      alert("Please select a file before clicking 'Submit'");
    } else {
      shapsFile = input.files[0];
      fr = new FileReader();
      fr.onload = parseFile;

      fr.readAsText(shapsFile);
    }

}

// grap the raw text from the file and convert into polylines and place them in sessionStorage
function parseFile(){
  console.log("ParseFile");
  filetype = document.getElementById("fileType").value;
  if ( filetype === null ) return;
  if ( filetype === "GeoJSON" ){

    GeoJSONFile = JSON.parse(fr.result);

    // console.log("KMLfile = " + JSON.stringify(GeoJSONFile,null,2));

    var features = GeoJSONFile.features; // return array of JSON, entries are diff polylines
    var coordinates = null;
    for (var i = 0; i < features.length; i++){
      coordinates = features[i].geometry.coordinates; // return array of arrays with [lon, lat] as indecies
      // reverse the order of the the coordinates
      for (var j = 0; j < coordinates.length; j++){
        // console.log("coordinates["+j+"] = " + coordinates[j])
        coordinates[j] = [ coordinates[j][1], coordinates[j][0] ] // switch them around
      }
      // console.log("coordinates = " + coordinates);
      sessionStorage.setItem('polyline'+i, coordinates);
    }
  } else if ( filetype === "KML" ){

      KMLfile = fr.result;

      // console.log(KMLfile);

      var x2js = new X2JS();    // https://github.com/abdmob/x2js

      var jsonObj = x2js.xml_str2json(KMLfile);
      // console.log("jsonObj = " + JSON.stringify(jsonObj,null,2));

      var arr = jsonObj.kml.Document.Folder.Placemark;

      var polylines = []
      var line = null;
      var point = null;
      var lat = null;
      var lon = null;
      var point = null;
      var pointObj = null;
      var lineObjs = []

      for(var i = 0; i < arr.length; i++){
        // console.log("arr["+i+"] = " + JSON.stringify(arr[i],null,2));
        line = arr[i].MultiGeometry.LineString.coordinates.split("0 ");
        for (var j = 0; j < line.length; j++){
          point = line[j].split(",")
          // lat is second number
          // console.log("point = " + JSON.stringify(point));
          lat = point[1];
          // lon is first number
          lon = point[0];
          pointObj = [ lat, lon ]
          lineObjs.push(pointObj);

        }
        console.log("lineObjs = " + lineObjs);
        sessionStorage.setItem('polyline'+i, lineObjs);
        lineObjs = [];
      }

      // save in sessionStorage object for use in other
      sessionStorage.setItem('Shape', polylines);
  }
}

// change the file type needed
function onChangeFileType() {
  // ge the DOM objects for manipulation
  var inputItem = document.getElementById("fileType")
  var submitItem = document.getElementById("fileSubmit")

  // get the item the user selected
  fileType = inputItem.value;

  // change what file will be accepted by the
  if ( fileType === "KML"){
    submitItem.accept = ".kml";
  } else if ( fileType === "GeoJSON"){
    submitItem.accept = ".json";
  }
  console.log("Changed fileType. It is now " + fileType + ".");
}
