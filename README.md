# transit-editor #

## Purpose ##

Transit-Editor uses leaflet.js to provide a visual editor for creating and maintaining GTFS feeds (see more: https://developers.google.com/transit/gtfs/).

## Installation ##

To run this appliction follow these steps:

1. Clone this repository on to your local computer

  ```
  $ git clone https://Jakecrews74@bitbucket.org/apgisdev/transiteditor.git
  ```

2. Change directory to the clone repo folder:

  ```
  $ cd transiteditor
  ```

3. Install ALL npm packages

  ```
  $ npm install
  ```

4. Open index.html to view home page


## Versions ##

* [npm](http://blog.npmjs.org/post/85484771375/how-to-install-npm) v3.10.10.

